= Anexo 24 Proceso de Queja por Derechos Civiles y ADA/Sección 504 ante la DFCS
:navtitle: Attachment 24 DFCS Civil Rights and ADA/Section 504 Complaint Process (Spanish)
:lang: es
:revision-date: 3/3/2022
:note-caption: NOTA

== Propósito

Estos procedimientos se han establecido para corregir cualquier violación del Título VI de la Ley de Derechos Civiles de 1964, en su forma enmendada, 42 U.S.C. 2000d ("Título VI"), Título IX de las Enmiendas de Educación de 1972, 20 U.S.C. 1681 _y los siguientes_, ("Título IX"), Sección 504 de la Ley de Rehabilitación de 1973, en su forma enmendada, 29 USC nro. 794 ("Sección 504"), Título II de la Ley de Estadounidenses con Discapacidades de 1990 ("ADA") y la Ley de Enmiendas de la ADA de 2008, 42 U.S.C. 12101 _y los siguientes_, la Ley de Discriminación por Edad de 1975, 42 USC, 6101 _y los siguientes_, ("Ley de Edad"), la Ley de Alimentos y Nutrición de 2008, modificada, el Programa Suplementario de Asistencia Nutricional (SNAP), 7 USC, 2011 _y los siguientes_, y las disposiciones, directivas y reglamentos de aplicación que rigen la administración de programas de asistencia infantil y asistencia de bienestar público de la DFCS.
En la medida en que estos procedimientos entren en conflicto con las leyes, reglamentos o directivas aplicables, dichas leyes, reglamentos y directivas serán de control.
Este Proceso de quejas no se aplica a las quejas de discriminación en el empleo bajo el Título VII de la Ley de Derechos Civiles de 1964, en su forma enmendada.

== Derecho a Presentar una Queja por Derechos Civiles o ADA/Sección 504

Todos los clientes de la División de Servicios para la Familia y el Niño de Georgia ("DFCS") tienen derecho a presentar una queja de discriminación por motivos de raza, color, origen nacional, discapacidad, edad, sexo y, en algunos casos, creencias raciales o políticas, o por represalias por participar en actividades previas de derechos civiles.
El Departamento de Agricultura de los Estados Unidos ("USDA") y el Departamento de Salud y Servicios Humanos de los Estados Unidos ("HHS") prohíben represalias por actividades previas de derechos civiles.
Ningún personal o contratista del Departamento de Servicios Humanos ("DHS") tomará represalias contra un demandante o su designado por presentar una queja de derechos civiles o contra cualquier persona que testifique o ayude en una investigación o participe en cualquier otra actividad protegida de derechos civiles.
Además, la utilización de este procedimiento de queja no es un requisito previo para presentar una queja ante el USDA o el Departamento de Salud y Servicios Humanos de los Estados Unidos ("HHS").

La DFCS debe asegurarse de que los avisos públicos de derecho a presentar una queja por discriminación se publiquen de acuerdo con las regulaciones federales y las guías en las políticas.
Por ejemplo, la Declaración de No Discriminación del USDA, incluida una declaración conjunta USDA-HHS para SNAP, se encuentra en https://www.fns.usda.gov/fns-nondiscrimination-statement.
El USDA también requiere la exhibición del cartel “Y justicia para todos” en lugares prominentes donde haya una presencia del USDA y donde puede ser leído por los clientes.

NOTE: Un "demandante", como se utiliza en este documento, es una persona que hace una queja verbal o escrita alegando discriminación ilegal.
La DFCS tiene prohibido divulgar Información de identificación personal (PII) o Información médica protegida (PHI) a personas no autorizadas.
Por lo tanto, la DFCS no divulgará ni permitirá el acceso a la PII o PHI del demandante sin la autorización apropiada.
En situaciones en las que un acompañante u otra persona solicita una modificación razonable o asistencia de comunicación en nombre de una persona con una discapacidad, la DFCS se pondrá en contacto con la persona con una discapacidad o representante autorizado para aclarar la solicitud.

== Denegación de Solicitud de Modificaciones Razonables bajo la ADA/Sección 504

Una solicitud de Modificación Razonable ayuda auxiliar o servicio para personas calificadas con discapacidades solo puede ser denegada por el Director de la División de la DFCS o su designado.

== Procedimiento para Presentar Quejas de Derechos Civiles y ADA/Sección 504

=== Presentar una Queja del Programa de Discriminación con Agencias Federales

Notifíquese al público que cualquier persona o representante puede presentar una queja verbal o escrita de discriminación con el HHS o el USDA por correo, fax o correo electrónico.
Consulte a continuación para obtener información sobre dónde presentar su queja de derechos civiles o ADA/Sección 504:

. *(HHS)* Servicios de Salud y Servicios Humanos de los Estados Unidos, Director de HHS, Oficina de Derechos Civiles, Sala 515-F, 200 Independence Avenue, S.W., Washington, D.C. 20201; o llame al (202) 619-0403 (voz) o (800) 537-7697 (TTY).

. *(USDA)* Departamento de Agricultura de los Estados Unidos, Oficina del Subsecretario de Derechos Civiles, 1400 Independence Avenue, SW, Washington, D.C. 20250-9410; o llame al (866) 632-9992, fax al (202) 690-7442 o correo electrónico a program.intake@usda.gov.
Servicio Federal de Retransmisión al (800) 877-8339; o Servicio Federal de Retransmisión en español al (800) 845-6136.

*Se puede acceder al Formulario de Queja de Derechos Civiles del USDA en línea en:*

Formulario en inglés:: https://www.usda.gov/sites/default/files/documents/USDA-OASCR%20P-Complaint-Form-0508-0002-508-11-28-17Fax2Mail.pdf

Formulario en español:: https://www.usda.gov/sites/default/files/documents/USDAProgramComplaintForm-Spanish-Section508Compliant.pdf

=== Presentar Quejas de Discriminación en Programas de Asistencia Infantil y Asistencia Pública al DHS/DFCS

Cualquier persona o representante también puede presentar una queja verbal o escrita alegando discriminación ilegal *(incluso si un cliente no está de acuerdo con las decisiones tomadas con respecto a las solicitudes de modificaciones razonables, ayudas auxiliares o servicios, o si un cliente cree que la DFCS no proporcionó una modificación razonable solicitada o asistencia de comunicación bajo la ADA/Sección 504) poniéndose en contacto con su oficina local de la DFCS o con una de las siguientes oficinas de DHS/DFCS:*

. *En el caso de las denuncias por discriminación basadas en el origen nacional (por ejemplo, no brindar un intérprete para las personas con dominio limitado del inglés), remitirlas al:*
+
[%hardbreaks]
Program Manager, Limited English Proficiency and Sensory Impairment Program
Georgia Department of Human Services, Office of General Counsel
47 Trinity Avenue SW
Atlanta, GA 30334
LEPSI@dhs.ga.gov
(877) 423-4746
*Las personas que tienen una discapacidad auditiva o del habla pueden llamar al 711 para que un operador se conecte con nosotros.*

. *Para las quejas basadas en raza, color, origen nacional (no LEP), edad, sexo, religión, creencias políticas o discapacidad (incluidas las quejas sobre las decisiones tomadas en relación con las solicitudes de ayudas o servicios auxiliares, asistencia de comunicación y modificaciones razonables en virtud de la ADA/Sección 504), remitirlas a la:*
+
[%hardbreaks]
Division of Family and Children Services
DFCS Civil Rights, ADA/Section 504 Coordinator
Georgia Department of Human Services, Office of General Counsel
47 Trinity Avenue SW
Atlanta, GA 30334
(877) 423-4746
*Las personas que tienen una discapacidad auditiva o del habla pueden llamar al 711 para que un operador se conecte con nosotros.*

=== Asistencia de Comunicación

Los clientes tienen derecho a asistencia de comunicación gratuita.
El personal del DHS/DFCS debe facilitar intérpretes e información traducida al comunicarse con personas con dominio limitado del inglés y ayudas y servicios auxiliares para personas con discapacidades.

== Período de Tiempo para Presentar Quejas

Una queja se presenta oportunamente si se presenta dentro de los *180 días corridos* siguientes al supuesto acto discriminatorio o si alega que el acto discriminatorio está en curso.
El Secretario de Agricultura o el Secretario de Salud y Servicios Humanos pueden aceptar quejas presentadas después de la fecha límite de *180 días corridos* si el demandante puede presentar una explicación de "buena causa" para el retraso.
Por lo tanto, el Programa de Derechos Civiles de la DFCS enviará cualquier queja que no cumpla con el plazo de 180 días calendario a la agencia federal apropiada para su consideración.
El tiempo para presentar una queja ante el HHS o USDA no se rige por esta política.

=== Presentación de Quejas Escritas

Las quejas escritas pueden presentarse al DHS/DFCS a través de la entrega en mano, facsímil, correo electrónico, correo electrónico de los Estados Unidos u otro servicio de entrega utilizando el Formulario 724 de la DFCS (es decir, los Derechos Civiles de DFCS, ADA/Sección 504 del Formulario de Quejas).
El personal de la DFCS está obligado a dar un Formulario 724 a un cliente de la DFCS que solicite dicho formulario.
El Formulario 724 de DFCS no está obligado a presentar una queja por escrito.
Una queja también puede ser presentada por carta o correo electrónico si ese es el método de comunicación preferido del demandante/representante.
Además, notifique al demandante que puede presentar una queja ante el USDA o el HHS.

=== Presentación de Quejas Verbales

Si un demandante o representante presenta las alegaciones verbalmente o en persona, la persona del personal de la DFCS ante la que se hacen las acusaciones debe escribir los elementos de la queja utilizando el Formulario 724 de la DFCS.
Como mínimo, la persona del personal de DFCS debe obtener la siguiente información:

. Nombre, dirección y número de teléfono u otros medios para ponerse en contacto con la persona que alega discriminación;
. La ubicación y el nombre del condado, regional u otra oficina que presta el servicio o beneficio;
. La naturaleza o el incidente o acción que llevó al demandante o a su representante a sentir que la discriminación era un factor, y un ejemplo del método de administración que está teniendo un efecto dispar en el público, las posibles personas elegibles, los solicitantes o los participantes;
. La base sobre la que el demandante cree que existe discriminación;
. Los nombres, números de teléfono, títulos y direcciones comerciales o personales de las personas que puedan tener conocimiento de la supuesta acción discriminatoria; y
. La(s) fecha(s) durante la cual se produjeron las supuestas acciones discriminatorias, o si continúan, la duración de tales acciones.

NOTE: La persona del personal de la DFCS ante la que se presentan las denuncias de discriminación están obligadas a ayudar a una persona o a su representante (si se solicita) a gestionar el proceso de queja, que incluye completar el Formulario 724 de la DFCS y brindar información en formatos e idiomas alternativos, a pedido.
La DFCS debe garantizar que los formularios de quejas traducidos, los intérpretes calificados y las ayudas y servicios auxiliares estén disponibles de forma gratuita para los clientes de la DFCS y sus acompañantes.

== Supervisión y Remisión del Procesamiento de Quejas de Derechos Civiles y ADA/Sección 504

El DHS y la DFCS son responsables de garantizar que todas las quejas por discriminación presentadas ante la DFCS para cualquier programa del USDA o HHS, incluidas las quejas presentadas ante el Programa LEP/SI del DHS, se procesen de acuerdo con las regulaciones, directivas y orientación del procesamiento de quejas del USDA y HHS.

Esta subsección describe el proceso para los derechos civiles, la recepción de quejas de ADA/Sección 504, las referencias de quejas, el procesamiento, las investigaciones y las decisiones (incluido el derecho del demandante a apelar).
El proceso para la Oficina de Independencia Familiar ("OFI") y las quejas relacionadas con el bienestar infantil se describe a continuación.

=== Recepción de Quejas

Todas las quejas de derechos civiles y de ADA/Sección 504 deben ser remitidas *dentro de los cinco (5) días hábiles siguientes a la recepción* a los Derechos Civiles de la DFCS, al Coordinador estatal de la ADA/Sección 504.
Las quejas deben registrarse en un sistema de seguimiento manual o computarizado y mantenerse separadas de las quejas del programa.
Las quejas anónimas deben ser procesadas como cualquier otra queja, en la medida de lo posible, basada en la información disponible.

El Coordinador o personal designado de derechos civiles y ADA/Sección 504 de la DFCS, revisará una queja para ver si contiene una denuncia de discriminación sobre la base de una clase protegida o represalias.
Las quejas que no contienen tales denuncias serán enviadas a la oficina apropiada que registra y procesa las quejas del programa.

=== Reconocer una Queja (Solo para el Programa USDA SNAP)

*Dentro de los cinco (5) días hábiles siguientes a la recepción de una queja*, el Coordinador de Derechos Civiles de la DFCS, el Coordinador de la ADA/Sección 504 o su designado deben enviar una carta de reconocimiento al demandante.
Como mínimo, la carta de reconocimiento debe informar al demandante de cualquier acción planificada, el plazo para completar la investigación, y solicitar información adicional, si es necesario.

=== Referencias de Quejas

==== Programa de Alimentos Suplementarios de Productos Básicos (CSFP) del USDA FNS y Programa de Asistencia Alimentaria de Emergencia (TEFAP)

El DHS/DFCS debe remitir todas las quejas presentadas en el CSFP y TEFAP al Oficial Regional de Derechos Civiles de la FNS para su procesamiento dentro de los *cinco (5) días corridos siguientes* a la recepción de la queja.

==== Quejas que Alegan Discriminación Basada en la Edad en los Programas de HHS y USDA

Las regulaciones federales que aplican la Ley de Discriminación por Edad de 1975, exigen que todas las quejas que alegan violaciones de la Ley sean derivadas a mediación.
La DFCS debe remitir todas las quejas alegando discriminación por edad, independientemente de si se alegan otras bases, a la División de Derechos Civiles (CRD) del Servicio de Alimentos y Nutrición del USDA (FNS), Oficial Regional de Derechos Civiles, o a la Oficina de Derechos Civiles del HHS para su procesamiento.

Se deben reenviar todas las quejas que aleguen discriminación ilegal por razón de la edad, *independientemente de si se alegan otras causas*, a la Oficina Regional de Derechos Civiles del HHS o a la División de Derechos Civiles del FNS, Oficial Regional de Derechos Civiles *dentro de los cinco (5) días hábiles*.

Si la mediación del Servicio Federal de Mediación y Conciliación ("FMCS") es exitosa, el FMCS notificará a la agencia federal apropiada para que el caso pueda ser cerrado.
La DFCS recibirá una notificación del cierre de la queja.
Si la mediación no tiene éxito, el FMCS remitirá la queja al USDA o al HHS para su procesamiento a través del sistema de procesamiento de quejas establecido.

La DFCS debe participar en la mediación sobre una base de buena fe para resolver las quejas que alegan discriminación ilegal sobre la base de la edad de conformidad con 45 CFR 90 y 7 C.F.R. 15c(7)(d).
El representante designado por la DFCS para participar en la mediación debe ser una parte neutral.
Una persona nombrada en la queja no es una parte neutral.

=== Procesamiento e Investigación de Quejas

==== Resumen

Cada decisión emitida por el DHS/DFCS incluirá un aviso de derechos de apelación.
El lenguaje de los derechos de apelación aparece en la Sección E.

Excepto para las quejas que involucran un dominio limitado del inglés (LEP), *dentro de (14) días hábiles a partir de* la recepción de una queja, el Coordinador de Derechos Civiles de la DFCS, ADA/Sección 504 trabajará con la administración del Estado, Regional/Distrito o Condado para asignar a una persona calificada del personal para investigar el caso.
El investigador asignado debe completar la investigación *dentro de los 45 días hábiles siguientes a la fecha de la queja*.
El investigador debe reunir y evaluar hechos que respalden o refuten las alegaciones del demandante, asegurándose de que se investiguen todas las fuentes de información pertinentes, lo que incluye una revisión del expediente del caso del demandante.
Las quejas limitadas por dominio del inglés serán investigadas por el Gerente del Programa del DHS para el Dominio Limitado del Inglés.

El investigador debe enviar los hallazgos de la investigación al Coordinador de Derechos Civiles de la DFCS, ADA/Sección 504, quien evaluará el informe para obtener la integridad y solidez de las conclusiones.
Si las conclusiones no contienen información suficiente para apoyar la conclusión de la investigación, el Coordinador de Derechos Civiles de la DFCS, ADA/Sección 504 devolverá las conclusiones al investigador con un plazo para revisarlas.

*Dentro de los 90 días siguientes a la aceptación de la queja para la investigación*, la DFCS redactará una carta de decisión que aplique la ley apropiada a los hechos pertinentes (y seguirá los requisitos de la Instrucción 113-1 del FNS para investigar las quejas de SNAP).
Si la queja es con respecto a un programa OFI y la ADA/Sección 504, el Coordinador de Distrito de la DFCS ADA/Sección 504 emitirá una decisión final por escrito.
Si la queja es relacionada con el dominio limitado del inglés, el Gerente del Programa LEP del DHS emitirá la decisión final por escrito.
Para todas las demás quejas de derechos civiles, los Derechos Civiles de la DFCS, el Coordinador de la ADA/Sección 504 emitirá la decisión final por escrito.

NOTE: Para SNAP, consulte la Instrucción FNS 113-1, Sección XV para obtener información específica sobre la recopilación de pruebas en una queja por discriminación.
Para las quejas de SNAP, antes de emitir una carta de decisión al
demandante, el DHS y la DFCS deben presentar al Oficial Regional de Derechos Civiles del FNS (RCRO) el proyecto de carta de decisión e informe de investigación con fines de supervisión y concurrencia (Instrucción FNS 113-1, XVI. C.8).
Si el FNS RCRO no está de acuerdo, el FNS RCRO tomará nota de cualquier motivo de no concurrencia y lo regresará a la DFCS para que se puedan abordar los problemas identificados.
Tras la concurrencia de FNS RCRO, la DFCS enviará la carta de decisión final al demandante en un plazo de *20 días corridos*.
Las cartas de decisión incluirán el derecho del demandante a apelar ante el Secretario de Agricultura.
(Véase la Instrucción FNS 113-1, XVI. C.6).
Si no se lleva a cabo una investigación, el organismo estatal dará una explicación en su informe al Oficial Regional de Derechos Civiles del FNS (Instrucción FNS 113-1, XVII. D.4.d).

==== Procesamiento Adicional para Quejas de OFI ADA/Sección 504

Las quejas de la OFI ADA/Sección 504 serán revisadas e investigadas por el Coordinador de Distrito de DFCS ADA/Sección 504.
El Coordinador de Distrito de DFCS ADA/Sección 504 debe enviar las conclusiones de la investigación a los Derechos Civiles de la DFCS, al Coordinador del Estado de la ADA/Sección 504, quien evaluará el informe para obtener información sobre la integridad y la solidez de las conclusiones.
Si las conclusiones no contienen información suficiente para apoyar la conclusión de la investigación, el Coordinador Estatal de Derechos Civiles de la DFCS, ADA/Sección 504 devolverá los hallazgos al Director del Condado DFCS ADA/Sección 504 Coordinador estatal con un plazo para revisar las conclusiones.
El Coordinador de Estado de DFCS Civil Rights, ADA/Section 504 revisará la decisión antes de que sea definitiva.
El Coordinador de Distrito de DFCS Civil, ADA/Sección 504 emitirá una decisión final por escrito sobre las quejas.

=== Derecho del Demandante a Apelar la Decisión Final del DHS/DFCS con Respecto a una Queja de Derechos Civiles o ADA/Sección 504

El Coordinador de Derechos Civiles de la DFCS, el Coordinador del Estado de ADA/Sección 504, el Gerente del Programa LEP (para quejas LEP) o el Coordinador de Distrito de la ADA/Sección 504 de la DFCS (para las quejas de ADA/Sección 504) deben enviar al demandante una carta informándole de los hallazgos de investigación.
La carta también debe informar al demandante que, si no está de acuerdo con los resultados de la investigación, se puede presentar una apelación ante la Oficina del Subsecretario de Derechos Civiles del Departamento de Agricultura de los Estados Unidos o ante la Oficina de Derechos Civiles del Departamento de Salud y Servicios Humanos de los Estados Unidos.
La carta también debe incluir los derechos de apelación para cada oficina.

En cada carta de decisión final, el DHS/DFCS notificará al demandante su derecho a apelar dicha decisión (incluidas las decisiones sobre las quejas de ADA/Sección 504 tomadas con respecto a solicitudes de ayudas y servicios auxiliares o modificaciones razonables o no brindar ayuda y servicios auxiliares solicitados o una modificación razonable).
La información de las apelaciones debe proporcionarse en un formato o idioma alternativo que el demandante pueda entender.

==== Lenguaje de Apelación para Quejas de USDA de SNAP

Para apelar las decisiones que involucren servicios, programas o actividades de la DFCS financiados por el Departamento de Agricultura de los Estados Unidos (por ejemplo, SNAP), el demandante puede apelar a la Oficina del Subsecretario de Derechos Civiles del Departamento de Agricultura de los Estados Unidos dentro de los *90 días siguientes a la recepción de la carta de decisión final de la DFCS*.
La siguiente frase debe incluirse en todas las cartas de decisión emitidas a menos que sea necesaria una modificación para dar un aviso en un formato accesible que el demandante pueda entender:

Si no está de acuerdo con esta decisión, puede apelar a la Oficina del Subsecretario de Derechos Civiles del Departamento de Agricultura de los Estados Unidos.
Debe hacerlo *dentro de los 90 días posteriores a la recepción de esta carta*.
Para apelar esta decisión, escriba al:

[%hardbreaks]
U.S. Department of Agriculture
Office of the Assistant Secretary for Civil Rights,
Stop 9430
1400 Independence Avenue, S.W.
Room 212-A Whitten Building
Washington, D.C., 20250

==== Formato de Apelaciones para Quejas de HHS

Para apelar decisiones que involucren servicios, programas o actividades de la DFCS financiados por el Departamento de Salud y Servicios Humanos de los Estados Unidos ("HHS") (por ejemplo, bienestar infantil y Asistencia Médica (por ejemplo, Medicaid y TANF), el demandante puede apelar a la Oficina de Derechos Civiles del HHS *dentro de los 90 días de recibir la carta de decisión final de DFCS*.
La apelación debe dirigirse a:

[%hardbreaks]
Office for Civil Rights
Centralized Case Management Operations
U.S. Department of Health and Human Services
200 Independence Avenue, S.W.
Habitación 509F HHH Bldg.
Washington, D.C. 20201

=== Resolución de Acciones de Incumplimiento / Corrección

Al finalizar la investigación, el investigador trabajará con el Condado y/o el Distrito o la Administración Regional para recomendar medidas correctivas (si corresponde).
Si se determina que el demandante fue discriminado sobre la base de una de las clases protegidas, o que el personal de la DFCS violó los requisitos estatales o federales, el Coordinador de Derechos Civiles, ADA/Sección 504 trabajará con la administración estatal, regional y del condado y el equipo de Control de Calidad de SNAP de la DFCS (cuando corresponda) para asegurar que se prepare, implemente y supervise un plan de acción correctivo para abordar las violaciones.
La DFCS cumplirá con las regulaciones y directivas aplicables del USDA y HHS cuando se garantice que las acciones correctivas requeridas se logran de manera oportuna.

=== Seguimiento de Quejas y Mantenimiento de Archivos de Quejas

La Oficina Estatal de Derechos Civiles mantendrá un expediente central sobre todas las quejas de Derechos Civiles y ADA/Sección 504 tramitadas por la DFCS.
El programa LEP del DHS mantendrá un archivo central en todas las quejas de la DFCS LEP procesadas por el DHS.
Si se mantiene, los archivos de quejas del condado deben almacenarse separados de las quejas del programa en una ubicación central para su revisión por parte de la Unidad de Control de Calidad de DFCS, la Oficina Estatal de Derechos Civiles o las oficinas federales de Derechos Civiles.
Las quejas de derechos civiles y de ADA/Sección 504 presentadas en cualquier programa del USDA deben registrarse bajo un sistema de registro separado y distinto según la Instrucción 113-1 del FNS, Sección XVII.
Todas las quejas de derechos civiles deben mantenerse confidenciales y solo ser accesibles a personas autorizadas.

== Otros Procedimientos

Los procedimientos proporcionados en este documento son adicionales a, y no en lugar de, cualquier otro recurso disponible bajo la ley federal o estatal.

== Referencias

[%hardbreaks]
Título VI de la Ley de Derechos Civiles de 1964 (42 USC nro. 2000d)
Artículo 504 de la Ley de Rehabilitación de 1974 (29 USC nro. 794)
Título IX de las Enmiendas a la Educación de 1972, Ley Pública 92-318, en su forma enmendada (20 U.S.C., 1681 _y siguientes_)
Ley de Discriminación por Edad de 1975 (42 USC nro. 6101)
Título II de la Ley de Estadounidenses con Discapacidades (ADA) de 1990 (42 USC nro. 12132) en 28 C.F.R. Parte 35 y 28 C.F.R.
Título II de la Ley de Enmiendas de la ADA de 2008 (Ley Pública 110-325)
Regulaciones del Departamento de Salud y Servicios Humanos de los Estados Unidos en 45 C.F.R. Partes 80, 84 y 91
Cumplimiento de los Derechos Civiles del Servicio de Alimentos y Nutrición del USDA (Instrucción FNS 113-1)
La Ley de Alimentos y Nutrición de 2008, en su forma enmendada, Programa Suplementario de Asistencia Nutricional (SNAP), anteriormente el Programa de Cupones para Alimentos, 7 U.S.C. 2011 _y siguientes_.
Regulaciones del Departamento de Agricultura de los Estados Unidos en 7 C.F.R. Parte 15, 7 C.F.R. Parte 272, 7 C.F.R. Parte 250, 7 C.F.R. Parte 251 y 7 C.F.R. Parte 247
Rawlings et al., CAFN: 1:17-CV-01434-TWT (N.D. Ga. 2019) (Orden de consentimiento, ingresada el 4 de junio de 2019)

Esta lista no es exclusiva.