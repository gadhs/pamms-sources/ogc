= Section II Needs Assessment

[#organization-commitment]
== Organizational Commitment

The responsibility for carrying out DHS policy and procedures to ensure meaningful access for persons with limited English proficiency and equal access for persons with sensory impairment is shared as a whole through the joint effort of the LEP/SI Program, and the various DHS divisions and offices that provide direct benefits or services to constituents with LEP and SI.

Each Division that provides social services to constituents implements the Access Plan to ensure service delivery at the local level.
Local level will be defined by Divisions based on programmatic operations.

Representatives from the DHS Divisions assist with the implementation of the DHS LEP/SI Policy by serving on the DHS Language Access Team (LAT), Complaint Resolution Team (CRT), and as Language Access Coordinators (LAC).
An ad hoc Community Advisory Council (CAC) comprised of representatives from major ethnic and language groups, including the hearing and visually impaired, is convened as needed to respond to DHS LEP/SI issues/concerns at the local level.
The CAC provides feedback and recommends improvements to LEP/SI service delivery focusing on constituent and community perspectives.

[#lepsi-specific-responsibilities]
=== LEP/SI Program – Specific Responsibilities

The DHS LEP/SI Program Manager serves as the primary point of contact for implementing the DHS LEP/SI policy as well as the point of contact for state-level and federal-level compliance reviews.
Also, the DHS LEP/SI Program Manager responds to compliance issues and prepares reports to the various offices within DHS or to federal agencies as needed.

The DHS LEP/SI Program has the following specific responsibilities:

* Maintaining the DHS LEP/SI policy and procedures and keeping them current and relevant;

* Convening, supporting, and maintaining up-to-date contact information for the teams that assist with LEP/SI Program implementation, specifically:
** *Language Access Team (LAT)* which consists of state level representatives from all Divisions,
** *Complaint Resolution Team (CRT)* which is an ad hoc team of the Language Access Team whose role is to assist in the investigation of allegations of discrimination based on LEP or SI, and
** *Language Access Coordinators (LAC)* who serve as the focal point for LEP/SI at the local or regional level for each Division.

* Developing and providing roles and responsibility training for LAT and CRT;

* Overseeing, along with programs, the translation and printing of vital forms and documents into languages most often and significantly encountered in the state;

* Managing feedback regarding contracts for telephone interpreting services, sign language interpreter services, language testing services, interpreter/interpretation services, translator/translation services, and other statewide contracts that provide language access services to constituents with LEP/SI;

* Managing and maintaining the information provided by the county, regional, and state program offices;

* Receiving, reviewing, and investigating appropriate complaints of discrimination based on disability (except DFCS disability complaints which will be handled by the DFCS Civil Rights and ADA/Section 504 Coordinator) or national origin as they relate to language assistance.
Note: The DHS LEP/SI Program will process discrimination complaints based on national origin filed against any USDA FNS program or activity in accordance with FNS Instruction 113-1; and

* Maintaining adequate language assistance resources (i.e. securing contractors to provide interpreter and translation services, testing) and assisting program Divisions and Offices with language assistance in serving constituents with LEP/SI.

* Monitoring implementation of the Policy and Access Plan and the Secret Shopper program.

[#lat-specific-responsibilities]
=== Language Access Team (LAT) - Specific Responsibilities

The LAT consists of at least one state-level representative from each DHS division and office and is responsible for:

* Providing input into the design and implementation of products and services for constituents with LEP/SI from the perspective of each division and office represented;
* Serving as the state level single point of contact for their program/office and respective county level single point of contacts (CLSC);
* Keeping the DHS LEP/SI program informed of the status of LEP/SI services within each area;
* Ensuring the completion and review of communication assistance self-assessment processes; and
* Aiding with the implementation of the Access Plan if needed.

[#lac-specific-responsibilities]
=== Language Access Coordinators (LAC) - Specific Responsibilities

The LAC is designated by the regional director for each office for the Division of Child Support Services (DCSS).
There is no Language Access Coordinators for DAS or DFCS.
All LEP/SI related inquiries will be directed to the DAS or DFCS Language Access Team Member, respectively.
Each of the LACs will report to their respective LAT member.
The LAT member for DAS and DFCS will represent the interests for the state and local levels.
The LAC:

* Serves as single point of contact for each county, district or region on all issues dealing with services to constituents with LEP/SI;
* Ensures that the Access Plan is implemented, based on the LEP/SI policy and procedures as well as the language needs of the service area;
* Secures and/or assists divisions/offices in securing language resources (interpreters, translated documents, etc.);
* Ensures that the xref:attachment$attachment-14-free-interpretation-services-poster.docx[Notice of Free Interpretation Services Poster] is visible and that xref:attachment$attachment-3-i-speak-flashcard.docx[“I SPEAK” cards] are accessible;
* Provides oversight and monitoring to the provision of oral and written language services to constituents; and
* Assesses staff training and conducts or coordinates training to ensure that staff is prepared to provide meaningful language access to constituents with LEP and equal access to constituents with SI.

*DHS’s subrecipients should contact the LEP/SI Program Manager with questions regarding provision of language assistance services.
Gateway Community Partners should contact the Gateway Community Partner Program Manager or LEP/SI Program Manager for assistance.*

[#crt-specific-responsibilities]
=== Complaint Resolution Team (CRT) - Specific Responsibilities

The CRT is an ad hoc group of the Language Access Team (LAT) whose role is to assist in the investigation of allegations of discrimination based on national origin or disability received by DHS or its Divisions and Offices, with exception.
The DFCS ADA/Section 504, Civil Rights Coordinator investigates complaints alleging disability discrimination.
The CRT may also respond to inquiries made by the U.S. Department of Agriculture (USDA), Food and Nutrition Service, Civil Rights Division, or the U.S. Department of Health and Human Services (HHS), Office of Civil Rights, regarding complaints received against DHS that allege national origin or disability discrimination.

The CRT consists of LEP/SI staff, the Language Access Team (LAT) member from the affected Division and may include any related Language Access Coordinators.
The CRT will work together to investigate and respond to the complaint.
Complaints will be responded to within five business days.

NOTE: Complaints involving USDA Food and Nutrition programs must be handled in accordance with the FNS Instruction 113-1.

[#self-assessment-of-need]
== Self Assessment of Need for Language Access and SI Access

For language access, DHS, its local offices, and subrecipients must conduct routine assessments at least every five years of need considering at a minimum the following four factors:

[loweralpha]
. The number or proportion of LEP persons eligible to be served or likely to be encountered within the area serviced by DHS, its local offices and subrecipients.
. The frequency with which LEP persons come in contact with the program, benefit, service, or activity.
. The nature and importance of the program, benefit, service, or activity to the LEP person.
The more important the program, or the greater the possible consequences of the contact to the LEP persons the more likely language services are needed.
. The resources that are available and the costs of providing the language assistance service(s).

DHS, its local offices, and subrecipients will apply the four factors to the various kinds of contact that its offices will have with the public to assess language needs and will decide what reasonable steps it should take to ensure meaningful access for LEP persons.

Please see xref:attachment$attachment-1-language-access-self-assessment.pdf[Attachment 1] for the DHS Language Access Self-Assessment of Need Survey for a copy of the survey.
However, the survey will be conducted through the DHS online form located in the DHS LEP/SI SharePoint site which can be found at: https://forms.office.com/g/wMC9gZwPwk.

For SI access, DHS, its local offices, and subrecipients must conduct routine self-evaluation of need at least every five years considering, at a minimum, the following factors - see the https://www.ada.gov/pcatoolkit/chap3chklist.htm[ADA Tool Kit: Chapter 3, Addendum Checklist].

[#demographic-data]
== Demographic Data

In conjunction with the self-assessment of need, demographic data of LEP and SI constituents for the state of Georgia will be produced by DHS at least every 5 years based on U.S. Census data, at minimum, to determine language access needs throughout the state for LEP constituents and disability access needs for SI constituents.
Other programs and offices may supply other data to ensure meaningful access to services for LEP constituents and equally effective communication for SI constituents.
Data from the Decennial Census and/or the American Community Survey will be provided identifying LEP groups by language, population, and county.
Please see xref:attachment$attachment-2-census-language-spoken-at-home.xlsx[Attachment 2] for data related to LEP and SI constituents in the state of Georgia.