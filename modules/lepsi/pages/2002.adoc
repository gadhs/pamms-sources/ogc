= 2002 Bilingual / Multilingual Temporary Salary Supplement (TSS) Testing Policy
:policy-number: 2002
:policy-title: Bilingual / Multilingual Temporary Salary Supplement (TSS) Testing Policy
:revision-date: 11/23/2022
:next-review-date: 11/23/2024

include::ROOT:partial$policy-header.adoc[]

== Subject

The purpose of this policy is to govern a temporary salary supplement (TSS or supplement) issued to bilingual/multilingual client-facing employees and employees who assist with business operation needs and enhance the Department of Human Services’ ability to serve customers in their language of choice.
This policy guides testing eligibility, protocols, frequency, and other considerations related to bilingual/multilingual TSS.

== Policy

=== General Provisions

A TSS shall be paid to employees in positions where bilingual/multilingual proficiency assists the Department in providing excellent customer service to clients and constituents.

The designation of bilingual/multilingual required positions is the sole prerogative of the Department and is based on operational, staffing and budgetary needs.
Provisions of this policy applicable to DFCS only are noted accordingly.

=== Eligibility

Bilingual/Multilingual employees who wish to receive a TSS must meet minimum standards to be considered for the supplement.

[loweralpha]
. Individuals must be an employee in good standing (no performance plans or other performance-related documents of concerns on file within the last calendar year).
. Individuals must be in a primarily (more than 80% of work time) client-facing role that requires direct interaction with Limited English Proficient (LEP) customers and constituents.
Or, individuals may also be in a role which assists with business operation needs of the division that may require speaking in a language other than English.
. The Bilingual/Multilingual Supplement Request form must be completed and submitted to the DHS Limited English Proficiency/Sensory Impairment (LEP/SI) Program Manager or his/her designee when requested.

=== Requesting Participation

A bilingual/multilingual employee who wishes to receive a supplement and who meets the eligibility requirements (see Section 2) should notify their direct supervisor of their interest.

[loweralpha]
. The DHS LEP/SI Program Manager may provide the Bilingual/Multilingual Supplement Request form upon request.
(see Attachment A)

. Once the form is completed and approved by the employee’s manager, the form must be returned to the DHS LEP/SI Program Manager to review and approve or deny eligibility.

. *(Non-DFCS ONLY subsection)* Employees who have been denied approval can appeal to the division director with a needs-based justification.

. *(DFCS ONLY subsection)* Employees who do not hold a client-facing role will be denied:
+
[lowerroman]
.. Employees or their leadership may file an appeal on any denial of bilingual/multilingual supplement eligibility.
The appeal language must include a needs-based justification of the multilingual/bilingual supplement and compelling customer need.
.. Appeals will be considered on a case-by-case basis with the advisement of District Directors, District Managers and DHS LEP/SI Program Manager.
If a consensus cannot be reached, the appeal will be forwarded to the Chief Operating Officer or his/her designee.

=== Bilingual/Multilingual Testing Requirements

For an employee to receive a Bilingual/Multilingual Supplement, he/she must demonstrate language proficiency.
Proficiency is demonstrated via testing completed by an approved contracted vendor.

[loweralpha]
. Those interested in testing for bilingual/multilingual eligibility will have up to 30 calendar days to complete the online proficiency test from the date the testing link/schedule is provided by the DHS LEP/SI Program Manager.

. The Department will fund the employee’s first attempt at bilingual/multilingual testing.

. The employee must demonstrate competency to receive a supplement.
Currently, a passing score of 10 or above (out of 12) will demonstrate competency.

. Employees who fail to demonstrate proficiency in their selected language/s will be deemed ineligible.
Employees may be able to retest, however, the retest may be at the employee’s personal expense.

. Employees approved to receive a bilingual/multilingual supplement will be required to retest every five years from the date of their last successful test to measure continued proficiency.
Employees who fail to demonstrate proficiency in their selected language/s will be deemed ineligible, and their supplement will end at the next available pay period (1^st^ or 16^th^).
The employee’s HR liaison will need to submit a requisition to end the supplement.

=== Bilingual/Multilingual Supplement Processing

The Office of Human Resources (OHR) is responsible for the final approval and processing of the TSS, including bilingual/multilingual supplements.

[loweralpha]
. *(DFCS ONLY)* Once notified that an employee has passed the bilingual/multilingual proficiency test and meets all other requirements, the employee’s supervisor or designee, using established DFCS Staff Resource Management (SRM) protocols, should submit the bilingual/multilingual supplement request to the appropriate SRM mailbox.
SRM will complete their process and forward designated documentation to OHR, who will complete their portion of the process and provide final processing of the TSS.
Once processed, the employee will be notified by OHR, and the employee may begin providing language support services to agency clients and constituents.

== Authority

None

== References

None

== Applicability

This policy applies to all Divisions and Offices of the Department of Human Services.

== Definitions

Bilingual:: the ability to speak English and one other language.

Language:: refers to the method by which an individual communicates with another, in languages other than English, and other accepted means of communication used by customers with sensory impairments.

Limited English Proficient:: refers to persons who do not speak English as their native/primary language and who have a limited ability to read, speak, write or understand English.

Multilingual:: the ability to speak English and at least two other languages.

== Responsibilities

The Office of General Counsel – Limited English Proficiency/Sensory Impairment (LEP/SI) program is responsible for issuing and updating, as appropriate, procedures to implement this policy.
For additional information or assistance regarding language testing, please contact the LEP/SI office, or email lepsi@dhs.ga.gov.

The Office of Human Resources (OHR) is responsible for the final approval and processing of the TSS, including bilingual/multilingual supplements.
For additional information or assistance regarding TSS, please contact your local Human Resource Office, or email DHS-Policies@dhs.ga.gov.

== History

None

== Evaluation

The Office of General Counsel - LEP/SI program evaluates this policy by providing updates to the procedures to enhance programmatic operations and service delivery.